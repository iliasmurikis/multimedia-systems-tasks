clear;
close all;
% clc;
%-----------------------------------------------%
org = imread('photo.jpeg');
I = org;
Q=25;
[x,y,z] = size(I);
%-----------------------------------------------%
% Compression
disp('Compressing...')
I = I/Q;
round(I);
% Convert 3d array into 1d
ls = reshape(I,[1,x*y*z]);
% Apply run-length-encoding
[b,n] = RunLength(ls);
% n = uint8(n);
 disp('Compression finished.')
%-----------------------------------------------%
% Decompression
disp('Decompressing...')
% Applying run-length-decoding
decls = RunLength(b,n);
% Convert 1d array into 3d
newI = reshape(decls,[x,y,z]);

newI = newI*Q;

disp('Uncompressed image is ready.')
whos org
whos b
whos n
subplot(211),imshow(org),subplot(212),imshow(newI)
