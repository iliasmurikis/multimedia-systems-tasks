clc;    % Clear the command window.
close all;  % Close all figures (except those of imtool.)
imtool close all;  % Close all imtool figures.
clear;  % Erase all existing variables.

% Object to read the video
video = VideoReader('shuttle.avi');
% Get the total of frames in the video
numberOfFrames=video.numberOfFrames;
% Get the first 96 frames
numberOfFrames=numberOfFrames-25;
fontSize=14;  

% Display video as it is
for frame = 1 : numberOfFrames
		% Extract the frame from the movie structure.
		thisFrame = read(video, frame);
		imshow(thisFrame);
		caption = sprintf('Frame %4d of %d.', frame, numberOfFrames);  
		title(caption, 'FontSize', fontSize);
		drawnow; % Force it to refresh the window.
end
close all;

% Get the first frame from video as Iframe
Iframe = read(video, 1);

% Display the difference
for frame = 2 : numberOfFrames
		% Extract the Pframes from the movie structure.
		Pframe = read(video, frame);
		
		% Display the difference between the Iframe and the rest Pframes
		temp = imshowpair(Iframe,Pframe,'diff');
        
        caption = sprintf('Frame %4d of %d.', frame, numberOfFrames);  
		title(caption, 'FontSize', fontSize);
		drawnow; % Force it to refresh the window.
end
close all;

frame2 = read(video,2);
difference = frame2 - Iframe;
% Entropy of the second frame
disp(entropy(frame2));
% Entropy of the second frame - first frame
disp(entropy(difference));

%--------------------------------------------------------------------------------------------------%
% b)
% Run the prediction algorithm  
for frame = 2 : numberOfFrames
    Pframe = read(video,frame);
    [x_motion,y_motion,pf]=predict(Iframe,Pframe);
end

close all;

function [x_motion,y_motion,pf]=predict(Iframe,Pframe,plot_flag)
    % This function predict takes Iframe and Pframe and finds out
    % the motion vectors(x_motion & y_motion), predicted frame error (pf)
    % The default block size is taken as 16x16 and the search window as 16x16
    % first the motion vector(x_motion and y_motion) is calculated and then
    % the predicted frame error from such an reconstructed
    % frame is calculated. Plot_flag=1, plots the figures.

    N = 16;      % Block size is N x N pixels
    K = 32;     % Search window size is K x K pixels

    % Make image size divisible by 16
    [X,Y,~] = size(Pframe);
    if mod(X,N)~=0
        Height = ceil(X/N)*N;  % Cut off the extra rows if not divisible by N
    else
        Height = X;  % Else keep as it is
    end
    if mod(Y,N)~=0
        Width = ceil(Y/N)*N;   % Cut off the extra cols if not divisible by N
    else
        Width = Y;  % Else keep as it is
    end
    
    % Change the size of the I and P frame
    newIframe = zeros(Height,Width,3);
    newPframe = zeros(Height,Width,3);    
    % Add the black pixels
    newIframe(1:X,1:Y,1:3) = Iframe(:,:,:);
    newPframe(1:X,1:Y,1:3) = Pframe(:,:,:);
    % Convert images into integers
    Iframe = uint8(newIframe);
    Pframe = uint8(newPframe);
   
    clear X Y Z;
   
    % Pad input images on left, right, top, and bottom
    Iframe1 = double(padarray(Iframe,[K/2 K/2]));
    Pframe1 = double(padarray(Pframe,[K/2 K/2]));

    % Initialization of motion vector matrices
    x = zeros(Height/N,Width/N);    % x-matrix
    y = zeros(Height/N,Width/N);    % y-matrix

    % Determine Motion Vector for the image by calculating SAD
    for rows = N:N:Height
        rblk = floor(rows/N);   % Pointer for motion vectore x
         for cols = N:N:Width
            cblk = floor(cols/N);   % Pointer for motion vectore y
            SAD = 1.0e+10;  % Initial Sum Absolute Difference(SAD)
            for u = -N:N
                for v = -N:N
                    sad = Pframe1(rows+1:rows+N,cols+1:cols+N)-Iframe1(rows+u+1:rows+u+N,cols+v+1:cols+v+N);
                    sad = sum(abs(sad(:)));     % SAD between pixels
                    if sad < SAD    % min SAD
                        SAD=sad;
                        % Motion Vectors are positions in block, in which SAD is
                        % minimum
                        x(rblk,cblk) = v; y(rblk,cblk) = u; %Motion Vectors
                    end
                end
            end
        end
    end

    % Image reconstruction and Error calculation from motion vector
    x_motion=x;
    y_motion=y;    
    N2 = 2*N;
    pf = double(zeros(Height,Width)); % prediction frame Initialisation
    for rows = 1:N:Height
        rblk = floor(rows/N) + 1;
        for cols = 1:N:Width
            cblk = floor(cols/N) + 1;
            x1 = x(rblk,cblk); y1 = y(rblk,cblk); % take corresponding motion vector
            %Predicted frame error (Pframe - Iframe moved by motion vectors)
            pf(rows:rows+N-1,cols:cols+N-1) = Pframe1(rows+N:rows+N2-1,cols+N:cols+N2-1)-Iframe1(rows+N+y1:rows+y1+N2-1,cols+N+x1:cols+x1+N2-1);
        end
    end

    cla reset;
    imshow(pf,[]);
end