import cv2
import numpy as np

cap = cv2.VideoCapture('sample2.mp4')

# Check if camera opened successfully
if (cap.isOpened()== False):
	print("Error opening video stream or file")

videoList = []
# Read until video is completed
while(cap.isOpened()):
	# Capture frame-by-frame
	ret, frame = cap.read()
	if ret == False:
		width = int(cap.get(3))
		height = int(cap.get(4))
		break
	videoList.append(frame)

frames = (len(videoList))
Q = 4

# Create list with the frames of differences
d = []
d.append(videoList[0])
for i in range(1,frames):
	d.append(videoList[i] - d[i-1])

# Quantization
for i in range(0,frames):
	d[i] = (d[i]+Q // 2) // Q	# Implementing division and then round

# De-quantization
for i in range(0,frames):
	d[i] *=Q

fourcc = cv2.VideoWriter_fourcc(*'DIVX')
out = cv2.VideoWriter('output.avi', fourcc, 20.0, (width,height))

# Play the video lossy video
cv2.imshow('Video',d[0])
out.write(d[0])
for i in range(1,frames):
	temp = d[i]+d[i-1]
	out.write(temp)
	cv2.imshow('Video',temp)
	if cv2.waitKey(50) & 0xFF == ord('q'):
	  break

# When everything done, release the video capture object amd video writer object
cap.release()
out.release()
# Closes all the frames
cv2.destroyAllWindows()
